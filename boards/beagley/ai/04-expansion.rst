.. _beagley-ai-expansion:

Expansion
#########

.. todo::

   Describe how to build expansion hardware for BeagleY-AI

PCIe
****

For software reference, you can see how PCIe is used on NVMe HATs.

* :ref:`beagley-ai-expansion-nvme`
* :ref:`beagley-ai-imx219-csi-cameras`
* :ref:`beagley-ai-rtc`
