.. _beagley-ai-demos:

Demos and tutorials
####################

.. toctree::
   :maxdepth: 1

   demos/beagley-ai-expansion-nvme
   demos/beagley-ai-pca9685-motor-drivers
   demos/beagley-ai-arducam-imx219-v3link-dual-camera-kit

